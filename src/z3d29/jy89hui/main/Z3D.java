package z3d29.jy89hui.main;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import graphics.entities.Camera;
import graphics.entities.Entity;
import graphics.entities.Light;
import graphics.gui.GuiRenderer;
import graphics.gui.GuiTexture;
import graphics.models.TexturedModel;
import graphics.renderEngine.DisplayManager;
import graphics.renderEngine.RendererManager;
import graphics.textures.TerrainTexture;
import graphics.textures.TerrainTexturePack;
import physical.player.Player;
import physical.world.HeightmapGenerator;
import physical.world.Terrain;
import physical.world.World;
import z3d.jy89hui.toolbox.MousePicker;
import z3d.jy89hui.toolbox.Shape;
import z3d.jy89hui.toolbox.ZData;

public class Z3D {
	
	public static void main(String[] args) {
		
		/**
		 * SETUP
		 */
		
		DisplayManager.createDisplay();
		DisplayManager.setOpenGLParameters();
		Shape.prepareAllDataOnShapes();
		Random random = new Random(ZData.RANDOM_SEED);
		
		/**
		 * LIGHTS
		 */
		float sunPower = 1.0f;
		Light light1 = new Light(new Vector3f(1271, 1782, 9), new Vector3f(sunPower,sunPower,sunPower));
		/*Light light2 = new Light(new Vector3f(-50,40,-20), new Vector3f(5,5,5), new Vector3f(1,0.1f,0.001f));
		Light light3 = new Light(new Vector3f(0,100,-250), new Vector3f(0,0.5f,1));
		Light light4 = new Light(new Vector3f(0,500,-20), new Vector3f(1,1,1));*/
		//Light light1 = new Light(new Vector3f(1500, 150, 1500), new Vector3f(5,5,1f), new Vector3f(.3f,0.001f,0.00001f));
		Light light2 = new Light(new Vector3f(0,150,1500), new Vector3f(5,1,1), new Vector3f(.3f,0.001f,0.00001f));
		Light light3 = new Light(new Vector3f(1500,150,0), new Vector3f(1,5,1), new Vector3f(.3f,0.001f,0.00001f));
		Light light4 = new Light(new Vector3f(0,150,0), new Vector3f(1,1,5), new Vector3f(.3f,0.001f,0.00001f));
		List<Light> lights = new ArrayList<Light>();
		lights.add(light1);
		/*lights.add(light2);
		lights.add(light3);
		lights.add(light4);*/
		/**
		 * MIPMAP TEXTURE 
		 */
		
		TerrainTexture rTexture = new TerrainTexture(ZData.loader.loadTexture("mud"));
		TerrainTexture gTexture = new TerrainTexture(ZData.loader.loadTexture("grassFlowers"));
		TerrainTexture bTexture = new TerrainTexture(ZData.loader.loadTexture("Grass-1"));
		TerrainTexture blendMap = new TerrainTexture(ZData.loader.loadTexture("blendMap"));
		TerrainTexture backgroundTexture = new TerrainTexture(ZData.loader.loadTexture("grass3"));
		TerrainTexturePack tPack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
		
		/**
		 * WORLD / ENTITY CREATION
		 */
		World world = new World();
		RendererManager renderer = new RendererManager(ZData.loader);
		
		world.addTerrain(new Terrain(0,0,tPack,blendMap,new HeightmapGenerator()));
		
		
		/*world.addTerrain(new Terrain(-1,0,tPack, blendMap,"heightmapSeattle"));
		world.addTerrain(new Terrain(0,-1,tPack, blendMap,"heightmapSeattle"));
		world.addTerrain(new Terrain(-1,-1,tPack, blendMap,"heightmapSeattle"));*/
		Entity dragon = new Entity(new TexturedModel(Shape.MODEL_BUNNY.texturedModel), new Vector3f(0,0,-25),0,0,0,10);
		//world.addEntity(dragon);
		int entityDistributionSize = ZData.TERRAIN_GENERIC_SIZE;
		for(int i=0; i<200; i++){
			
			world.addEntity(new Entity(new TexturedModel(Shape.MODEL_TREE_PALM.texturedModel),
					new Vector3f((float) (Math.random()) * entityDistributionSize, 0f,
							(float) (Math.random()) * entityDistributionSize),
					0, 0, 0, (float)(Math.random()*10+5)));
			world.addEntity(new Entity(new TexturedModel(Shape.MODEL_TREE_LEAFY.texturedModel),
					new Vector3f((float) (Math.random()) * entityDistributionSize, 0f,
							(float) (Math.random()) * entityDistributionSize),
					0, 0, 0, (float)(Math.random()*12+6f)));
			world.addEntity(new Entity(new TexturedModel(Shape.MODEL_FERN.texturedModel), random.nextInt(4),
					new Vector3f((float) (Math.random()) * entityDistributionSize, 0f,
							(float) (Math.random()) * entityDistributionSize),
					0, 0, 0, 2));
			
		}
		for(int i=0; i<300; i++){
			world.addEntity(new Entity(new TexturedModel(Shape.MODEL_FLOWER.texturedModel),
					new Vector3f((float)(Math.random())*entityDistributionSize,
							0f,
							(float)(Math.random())*entityDistributionSize),0,0,0,3));
		}
		for(int i=0; i<300;i++) {
		world.addEntity(new Entity(new TexturedModel(Shape.MODEL_WEEDS.texturedModel),
				new Vector3f((float) (Math.random()) * entityDistributionSize/5, 0f,
						(float) (Math.random()) * entityDistributionSize/5),
				0, 0, 0, 4));
		}
		// Make sure they are placed on the ground
		for(Entity e : world.getEntities()){
			e.setRotation(world.getNormal(e.getPosition().x, e.getPosition().z));
			e.getPosition().setY(world.getHeight(e.getPosition().x, e.getPosition().z));
		}
		for (Light l : lights) {
			Entity dra = new Entity(new TexturedModel(Shape.MODEL_BUNNY.texturedModel), l.getPosition(),0,0,0,1);
			world.addEntity(dra);
		}
		Entity stall =  new Entity(new TexturedModel(Shape.SQUARE.texturedModel), new Vector3f(20,30,20),0,0,0,500);
		world.addEntity(stall);
		world.addEntity(new Entity(new TexturedModel(Shape.MODEL_STALL.texturedModel), light1.getPosition(),0,0,0,1));
		
		/**
		 * SETUP PLAYER / CAMERA / GUI
		 */
		Player player = (new Player(world, new TexturedModel(Shape.SQUARE.texturedModel), new Vector3f(10,0,0),0,0,0,1));
		Camera camera = new Camera(player);
		GuiRenderer guiRenderer = new GuiRenderer(ZData.loader);
		List<GuiTexture> guis = new ArrayList<GuiTexture>();
		GuiTexture tex1 = (new GuiTexture(ZData.loader.loadTexture("flower"), new Vector2f(0f,0f), new Vector2f(0.5f,0.5f)));
		//guis.add(tex1);
		world.setPlayer(player);
		MousePicker mousePicker = new MousePicker(camera, renderer.getProjectionMatrix());
		
		/**
		 * MAIN GAME LOOP
		 */
		float currentAngle=0f;
		while(!Display.isCloseRequested()){
			currentAngle+=0.0009f;
			
			/*for(Entity e : world.getEntities()){
				e.increaseRotation(0.1f, 0.1f, 0.1f);
			}*/
			dragon.increaseRotation(0f, 0.4f, 0);
			Vector3f lightPos = new Vector3f(player.getPosition());
			lightPos.y = player.getPosition().y+5;
			//light2.setPosition(lightPos);
			light1.getPosition().x = (float)Math.cos(currentAngle) * 5000f;
			light1.getPosition().y =  (float)Math.sin(currentAngle) * 5000f;
			light1.getColor().x = 1.6f;//(float)Math.abs(Math.cos(currentAngle))*-0.5f+1f;
			light1.getColor().y = 1.5f;//(float)Math.abs(Math.cos(currentAngle))*1f;
			light1.getColor().z = 1.5f;//(float)Math.sin(currentAngle) * 1f;
			if (light1.getPosition().y<0) {
				light1.setColor(new Vector3f(0,0,0));
			}
			stall.setPosition(light1.getPosition());
			
			camera.update();
			mousePicker.update();
			/*Vector3f ray = mousePicker.getCurrentRay();
			ray.x*=-5;
			ray.y*=5;
			ray.z*=-5;
			world.addEntity(new Projectile(world, new TexturedModel(Shape.MODEL_TREE_PINE.texturedModel),
					lightPos, ray,0,0,0,0.5f));*/
			world.performPhysicalUpdates();
			renderer.registerTerrains(world.getTerrains());
			renderer.registerEntities(world.getEntities());
			renderer.render(lights, camera);
			guiRenderer.render(guis);
			DisplayManager.updateDisplay();		

		}
		/**
		 * CLEANUP
		 */
		guiRenderer.cleanUp();
		renderer.cleanUp();
		ZData.loader.cleanUp();
		DisplayManager.closeDisplay();

	}

	
	
}
