package graphics.entities;

import org.lwjgl.util.vector.Vector3f;

import graphics.models.TexturedModel;

public class Entity {
	protected TexturedModel model;
	protected Vector3f position;
	protected float roll, yaw, pitch;
	protected float scale;
	private int textureIndex = 0;
	public Entity(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		this.model = model;
		this.position = position;
		this.pitch = rotX;
		this.yaw = rotY;
		this.roll = rotZ;
		this.scale = scale;
	}
	public Entity(TexturedModel model, int textureIndex, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		this.model = model;
		this.textureIndex=textureIndex;
		this.position = position;
		this.pitch = rotX;
		this.yaw = rotY;
		this.roll = rotZ;
		this.scale = scale;
	}
	public float getTextureXOffset(){
		int column = textureIndex % model.getTexture().getNumberOfRows();
		return (float)column/(float)model.getTexture().getNumberOfRows();
	}
	public float getTextureYOffset(){
		int row = textureIndex/model.getTexture().getNumberOfRows();
		return (float)row/(float)model.getTexture().getNumberOfRows();
	}
	public void increasePosition(float dx, float dy, float dz){
		position.x+=dx;
		position.y+=dy;
		position.z+=dz;
	}
	public void increaseRotation(float pitch, float yaw, float roll){
		this.pitch+=pitch;
		this.yaw+=yaw;
		this.roll+=roll;
	}
	public void setRotation(float pitch, float yaw, float roll) {
		this.pitch=pitch;
		this.yaw=yaw;
		this.roll=roll;
	}
	public void setRotation(Vector3f vec) {
		this.pitch=vec.x;
		this.yaw=vec.y;
		this.roll=vec.z;
	}
	public TexturedModel getModel() {
		return model;
	}
	public void setModel(TexturedModel model) {
		this.model = model;
	}
	public Vector3f getPosition() {
		return position;
	}
	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getRoll() {
		return roll;
	}
	public void setRoll(float roll) {
		this.roll = roll;
	}
	public float getYaw() {
		return yaw;
	}
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	public float getPitch() {
		return pitch;
	}
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	public float getScale() {
		return scale;
	}
	public void setScale(float scale) {
		this.scale = scale;
	}
}
