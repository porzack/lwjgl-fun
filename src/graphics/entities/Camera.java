package graphics.entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import physical.player.Player;

public class Camera {
	private Vector3f position = new Vector3f(0,0,0);
	private float pitch;
	private float yaw;
	private float roll;
	private Player player;
	public Camera(Player p){
		player=p;
	}
	public void update(){
		this.position.x=player.getPosition().x;
		this.position.y=player.getPosition().y+Player.PLAYER_HEIGHT;
		this.position.z=player.getPosition().z;
		this.pitch=player.pitch;
		this.roll=player.roll;
		this.yaw=player.yaw;
	}
	public Vector3f getPosition() {
		return position;
	}
	public void setPosition(Vector3f position) {
		this.position = position;
	}
	public float getPitch() {
		return pitch;
	}
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	public float getYaw() {
		return yaw;
	}
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	public float getRoll() {
		return roll;
	}
	public void setRoll(float roll) {
		this.roll = roll;
	}
}
