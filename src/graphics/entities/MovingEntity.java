package graphics.entities;

import org.lwjgl.util.vector.Vector3f;

import graphics.models.TexturedModel;
import graphics.renderEngine.DisplayManager;
import physical.world.World;
import z3d.jy89hui.toolbox.ZData;

public abstract class MovingEntity extends Entity{

	protected Vector3f velocity = new Vector3f(0,0,0);
	protected World world;
	public MovingEntity(World world, TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super(model, position, rotX, rotY, rotZ, scale);
		this.world=world;
	}
	public MovingEntity(World world, TexturedModel model, Vector3f position, Vector3f velocity, float rotX, float rotY, float rotZ, float scale) {
		this(world, model, position, rotX, rotY, rotZ, scale);
		this.velocity=velocity;
	}
	public void setVelocity(float x, float y, float z){
		this.setVelocity(new Vector3f(x,y,z));
	}
	public void setVelocity(Vector3f vel){
		this.velocity=vel;
	}
	public void applyGravity() {
		this.increaseVelocity(0, -ZData.GRAVITY*(float)DisplayManager.getLastFrameTimeSeconds(), 0);
	}
	public void applyDrag() {
		applyDrag(ZData.DRAG);
	}
	public void applyDrag(float dragcoff) {
		this.decayVelocity(dragcoff*(float)DisplayManager.getLastFrameTimeSeconds());
	}
	public void increaseVelocity(float x, float y, float z){
		this.velocity.x+=x;
		this.velocity.y+=y;
		this.velocity.z+=z;
	}
	protected void multiplyVelocity(float ratio){
		velocity.x*=ratio;
		velocity.y*=ratio;
		velocity.z*=ratio;
	}
	protected void decayVelocity(float ratio) {
		velocity.x -= (velocity.x*ratio);
		velocity.y -= (velocity.y*ratio);
		velocity.z -= (velocity.z*ratio);
		
	}
	protected boolean applyVelocityToPosition(World world, boolean terrainColision){
		this.position.x+=velocity.x;
		this.position.y+=velocity.y;
		this.position.z+=velocity.z;
		if(!terrainColision){
			return false;
		}else{
			float terrainHeight = world.getHeight(position.x, position.z);
			if(position.y < terrainHeight){
				velocity.y=0;
				position.y=terrainHeight;
				return true;
			}
			return false;
		}
	}
	public abstract void move();
	

}
