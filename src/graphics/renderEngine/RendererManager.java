package graphics.renderEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import graphics.entities.Camera;
import graphics.entities.Entity;
import graphics.entities.Light;
import graphics.fileLoading.Loader;
import graphics.models.TexturedModel;
import graphics.skybox.SkyboxRenderer;
import graphics.terrain.TerrainShader;
import physical.world.Terrain;
import shaders.StaticShader;
import z3d.jy89hui.toolbox.ZData;

public class RendererManager {

		private Matrix4f projectionMatrix;
		public StaticShader shader = new StaticShader();
		private EntityRenderer entityRenderer;
		private TerrainRenderer terrainRenderer;
		private TerrainShader terrainShader = new TerrainShader();
		private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();
		private List<Terrain> terrains = new ArrayList<Terrain>();
		
		private SkyboxRenderer skyboxRenderer;
		
		public RendererManager(Loader loader){
			this.enableCulling();
			this.createPerspectiveProjectionMatrix();
			entityRenderer = new EntityRenderer(shader, projectionMatrix);
			terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
			skyboxRenderer = new SkyboxRenderer(loader,projectionMatrix);
		}
		public static void enableCulling(){
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glCullFace(GL11.GL_BACK);
		}
		public static void diableCulling(){
			GL11.glDisable(GL11.GL_CULL_FACE);
		}
		public void render(List<Light> lights, Camera camera){
			this.prepNextFrame();
			skyboxRenderer.render(camera, new Vector3f(ZData.SKY_R, ZData.SKY_G, ZData.SKY_B));
			shader.start();
			shader.loadSkyColor(ZData.SKY_R, ZData.SKY_G, ZData.SKY_B);
			shader.loadLights(lights);
			shader.loadViewMatrix(camera);
			entityRenderer.render(entities);
			shader.stop();
			
			terrainShader.start();
			terrainShader.loadSkyColor(ZData.SKY_R, ZData.SKY_G, ZData.SKY_B);
			terrainShader.loadLight(lights);
			terrainShader.loadViewMatrix(camera);
			terrainRenderer.render(terrains);
			terrainShader.stop();
			terrains.clear();
			entities.clear();
		}
		public void registerTerrain(Terrain terrain){
			terrains.add(terrain);
		}
		public void registerTerrains(List<Terrain> terrains){
			for(Terrain t : terrains){
				registerTerrain(t);
			}
		}
		public void registerEntities(List<Entity> entities){
			for(Entity e: entities){
				registerEntity(e);
			}
		}
		public void registerEntity(Entity entity){
			TexturedModel entityModel = entity.getModel();
			List<Entity> set = entities.get(entityModel);
			if(set!=null){
				set.add(entity);
			}else{
				List<Entity> set1 = new ArrayList<Entity>();
				set1.add(entity);
				entities.put(entityModel, set1);
			}
		}
		public Matrix4f getProjectionMatrix() {
			return projectionMatrix;
		}
		public void cleanUp(){
			shader.cleanUp();
			terrainShader.cleanUp();
		}
		public void prepNextFrame(){
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glClearColor(ZData.SKY_R, ZData.SKY_G, ZData.SKY_B, 1);
		}
		private void createPerspectiveProjectionMatrix(){
			float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
			float y_scale = (float) ((1f / Math.tan(Math.toRadians(ZData.FOV / 2f))) * aspectRatio);
			float x_scale = y_scale / aspectRatio;
			float frustum_length = ZData.FAR_PLANE - ZData.NEAR_PLANE;
			projectionMatrix = new Matrix4f();
			projectionMatrix.m00 = x_scale;
			projectionMatrix.m11 = y_scale;
			projectionMatrix.m22 = -((ZData.FAR_PLANE + ZData.NEAR_PLANE) / frustum_length);
			projectionMatrix.m23 = -1;
			projectionMatrix.m32 = -((2 * ZData.NEAR_PLANE * ZData.FAR_PLANE) / frustum_length);
			projectionMatrix.m33=0;
			}
}
