package graphics.renderEngine;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

import jutils.jy89hui.general.ApplicationInterface;
import jutils.jy89hui.general.logger;
import z3d.jy89hui.toolbox.ZData;

public class DisplayManager {

	
	
	public static void createDisplay(){	
		
		ContextAttribs attribs = new ContextAttribs(3,2)
		.withForwardCompatible(true)
		.withProfileCore(true);
		
		try {
			Display.setDisplayMode(new DisplayMode(ZData.WIDTH,ZData.HEIGHT));
			Display.create(new PixelFormat(), attribs);
			Display.setTitle("Z3D LWJGL29");
			Display.setFullscreen(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		GL11.glViewport(0,0, ZData.WIDTH, ZData.HEIGHT);
	}
	private static int counter =0;
	public static void updateDisplay(){
		if(counter++==10){
			logger.log("FPS: "+performFPSCalculations(10));
			counter=0;
		}
		Display.sync(ZData.FPS_CAP);
		Display.update();
		
	}
	
	public static void setOpenGLParameters() {
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
	}
	public static void closeDisplay(){
		
		Display.destroy();
		
	}
	private static long lastFPSTime = ApplicationInterface.getTime();
	private static float delta = 100;
	private static float performFPSCalculations(int framesPast){
		delta = ApplicationInterface.getDelta(lastFPSTime);
		delta/=framesPast;
		lastFPSTime=ApplicationInterface.getTime();
		return (float)(1000f/delta);
	}
	public static float getLastFrameTimeMS(){
		return delta;
	}
	public static float getLastFrameTimeSeconds(){
		return delta/1000;
	}
}
