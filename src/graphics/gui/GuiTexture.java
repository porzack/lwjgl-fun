package graphics.gui;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class GuiTexture {
	private int texture;
	private Vector2f position;
	private Vector2f scale;
	private Vector3f rotation = new Vector3f(0,0,0);
	public GuiTexture(int texture, Vector2f position, Vector2f scale, Vector3f rotation) {
		this.texture = texture;
		this.position = position;
		this.scale = scale;
		this.rotation = rotation;
	}
	public GuiTexture(int texture, Vector2f position, Vector2f scale) {
		this(texture,position,scale,new Vector3f(0f,0f,0f));
	}
	public int getTexture() {
		return texture;
	}
	public void setTexture(int texture) {
		this.texture = texture;
	}
	public Vector2f getPosition() {
		return position;
	}
	public void setPosition(Vector2f position) {
		this.position = position;
	}
	public Vector2f getScale() {
		return scale;
	}
	public void setScale(Vector2f scale) {
		this.scale = scale;
	}
	public Vector3f getRotation() {
		return rotation;
	}
	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
	}
	
}
