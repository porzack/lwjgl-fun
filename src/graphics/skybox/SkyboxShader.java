package graphics.skybox;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import graphics.entities.Camera;
import graphics.renderEngine.DisplayManager;
import shaders.ShaderProgram;
import z3d.jy89hui.toolbox.Tools;
import z3d.jy89hui.toolbox.ZData;

 
public class SkyboxShader extends ShaderProgram{
 
    private static final String VERTEX_FILE = "src/shaders/skyboxVertexShader.txt";
    private static final String FRAGMENT_FILE = "src/shaders/skyboxFragmentShader.txt";
     
    private float currentRotation=0f;
    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_fogColor;
    private int location_cubeMap1;
    private int location_cubeMap2;
    private int location_blendFactor;
     
    public SkyboxShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }
     
    public void loadProjectionMatrix(Matrix4f matrix){
        super.loadMatrix(location_projectionMatrix, matrix);
    }
 
    public void loadViewMatrix(Camera camera){
        Matrix4f matrix = Tools.createViewMatrix(camera);
        matrix.m30 = 0;
        matrix.m31 = 0;
        matrix.m32 = 0;
        currentRotation += ZData.SKY_ROTATION_SPEED * DisplayManager.getLastFrameTimeSeconds();
        Matrix4f.rotate((float)Math.toRadians(currentRotation), new Vector3f(0,1,0), matrix, matrix);
        super.loadMatrix(location_viewMatrix, matrix);
    }
     public void loadFogColor(Vector3f color) {
    	 	super.loadVector(location_fogColor, color);
     }
     public void loadBlendFactor(float factor) {
    	  super.loadFloat(location_blendFactor, factor);
     }
     public void connectTextureUnits() {
    	 	super.loadInt(location_cubeMap1,0);
    	 	super.loadInt(location_cubeMap2, 1);
     }
    @Override
    protected void getAllUniformLocations() {
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        location_fogColor = super.getUniformLocation("fogColor");
        location_cubeMap1 = super.getUniformLocation("cubeMap");
        location_cubeMap2 = super.getUniformLocation("cubeMap2");
        location_blendFactor = super.getUniformLocation("blendFactor");
    }
 
    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }
 
}

