package graphics.textures;

public class TerrainTexture {
	private int textureID;

	public TerrainTexture(int ID){
		this.textureID=ID;
	}
	public int getTextureID() {
		return textureID;
	}

}
