package graphics.models;

import z3d.jy89hui.toolbox.ZData;

public class RawModel {
	
	private int vaoID;
	private int vertexCount;
	
	public RawModel(int vaoID, int vertexCount){
		this.vaoID = vaoID;
		this.vertexCount = vertexCount;
	}
	public RawModel(ModelData md){
		this(ZData.loader.loadToVAO(md.getVertices(), md.getTextureCoords(), md.getNormals(), md.getIndices()));
	}
	public RawModel(RawModel other){
		this.vaoID=other.vaoID;
		this.vertexCount=other.vertexCount;
	}

	public int getVaoID() {
		return vaoID;
	}

	public int getVertexCount() {
		return vertexCount;
	}
	
	

}
