package graphics.models;

import graphics.textures.ModelTexture;

public class TexturedModel {
	private RawModel rawModel;
	private ModelTexture texture;
	
	public TexturedModel(RawModel model, ModelTexture texture){
		this.rawModel=model;
		this.texture=texture;
	}
	public TexturedModel(TexturedModel other){
		this.rawModel= new RawModel(other.rawModel);
		this.texture= new ModelTexture(other.texture);
	}

	public RawModel getRawModel() {
		return rawModel;
	}

	public ModelTexture getTexture() {
		return texture;
	}
}
