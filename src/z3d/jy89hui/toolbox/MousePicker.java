package z3d.jy89hui.toolbox;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import graphics.entities.Camera;

public class MousePicker {
	private Vector3f currentRay;
	
	private Matrix4f projectionMatrix;
	private Matrix4f viewMatrix;
	private Camera camera;
	
	public MousePicker(Camera camera, Matrix4f projectionMatrix) {
		this.setCamera(camera);
		this.setProjectionMatrix(projectionMatrix);
		this.setViewMatrix(Tools.createViewMatrix(camera));
	}
	public void update() {
		setViewMatrix(Tools.createViewMatrix(camera));
		setCurrentRay(calculateMouseRay());
		
	}
	private Vector3f calculateMouseRay() {
		float mousex = Mouse.getX();
		float mousey = Mouse.getY();
		Vector2f normalizedCoords = getNormalizedDeviceCoords(mousex, mousey);
		Vector4f clipCoords = new Vector4f(-normalizedCoords.x, normalizedCoords.y, -1f, 1f);
		Vector4f eyeCoords = toEyeCoords(clipCoords);
		Vector3f worldRay = toWorldCoords(eyeCoords);
		return worldRay;
	}
	private Vector3f toWorldCoords(Vector4f eyeCoords) {
		Matrix4f invertedView = Matrix4f.invert(viewMatrix, null);
		Vector4f rayWorld = Matrix4f.transform(invertedView, eyeCoords, null);
		Vector3f mouseray = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
		mouseray.normalise();
		return mouseray;
	}
	private Vector4f toEyeCoords(Vector4f clipCoords) {
		Matrix4f invertedProjection = Matrix4f.invert(projectionMatrix, null);
		Vector4f eyeCoords = Matrix4f.transform(invertedProjection, clipCoords, null);
		return new Vector4f(eyeCoords.x, eyeCoords.y, 1f, -0f);
	}
	private Vector2f getNormalizedDeviceCoords(float mousex, float mousey) {
		float x = (2f*mousex) / Display.getWidth() - 1f;
		float y = (2f*mousey) / Display.getHeight() - 1f;
		return new Vector2f(x,y);
	}
	public Vector3f getCurrentRay() {
		return currentRay;
	}

	public void setCurrentRay(Vector3f currentRay) {
		this.currentRay = currentRay;
	}

	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}

	public void setProjectionMatrix(Matrix4f projectionMatrix) {
		this.projectionMatrix = projectionMatrix;
	}

	public Matrix4f getViewMatrix() {
		return viewMatrix;
	}

	public void setViewMatrix(Matrix4f viewMatrix) {
		this.viewMatrix = viewMatrix;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}
}
