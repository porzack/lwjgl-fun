package z3d.jy89hui.toolbox;

import graphics.entities.Entity;
import graphics.fileLoading.OBJFileLoader;
import graphics.models.ModelData;
import graphics.models.RawModel;
import graphics.models.TexturedModel;
import graphics.textures.ModelTexture;

public enum Shape {
	SQUARE(new float[] {			
			-0.5f,0.5f,0,	//V0
			-0.5f,-0.5f,0,	//V1
			0.5f,-0.5f,0,	//V2
			0.5f,0.5f,0		//V3
			},
			new float[] {
			0,0,
			0,1,
			1,1,
			1,0
			},
			new int[] {
			0,1,3,	//Top left triangle (V0,V1,V3)
			3,1,2	//Bottom right triangle (V3,V1,V2)
	},null),
	MODEL_STALL(OBJFileLoader.loadOBJ("/model_stall/stall"), 
			new ModelTexture(ZData.loader.loadTexture("/model_stall/stallTexture"))),
	MODEL_DRAGON(OBJFileLoader.loadOBJ("/model_dragon/dragon"), null),
	MODEL_BUNNY(OBJFileLoader.loadOBJ("/model_bunny/bunny"), null),
	MODEL_TREE_PINE(OBJFileLoader.loadOBJ("/model_tree/treeLayered1/tree"), 
			new ModelTexture(ZData.loader.loadTexture("/model_tree/treeLayered1/tree"))),
	MODEL_TREE_SPHERES(OBJFileLoader.loadOBJ("/model_tree/treeSpheres1/tree"), 
			new ModelTexture(ZData.loader.loadTexture("/model_tree/treeSpheres1/tree"))),
	MODEL_FERN(OBJFileLoader.loadOBJ("/model_fern/fern"), 
			new ModelTexture(ZData.loader.loadTexture("/model_fern/fern"),2)),
	MODEL_WEEDS(OBJFileLoader.loadOBJ("/model_weeds/weed"), 
			new ModelTexture(ZData.loader.loadTexture("/model_weeds/weeds"))),
	MODEL_FLOWER(OBJFileLoader.loadOBJ("/model_weeds/weed"), 
			new ModelTexture(ZData.loader.loadTexture("/model_weeds/flower"))),
	MODEL_TREE_PALM(OBJFileLoader.loadOBJ("/model_tree/palmTree/Palma"), 
			new ModelTexture(ZData.loader.loadTexture("/model_tree/treeLayered1/tree"))),
	MODEL_TREE_LEAFY(OBJFileLoader.loadOBJ("/model_tree/Tree/Tree"), 
			new ModelTexture(ZData.loader.loadTexture("/model_tree/treeSpheres1/tree"))),
		
	;
	public RawModel rawModel;
	public ModelTexture modelTexture;
	public TexturedModel texturedModel;
	public Entity entity;
	Shape(float[] verticies, float[] textureCoords, int[] indicies, ModelTexture texture){
		rawModel = ZData.loader.loadToVAO(verticies, textureCoords, indicies);
		this.setup(rawModel, texture);
	}
	Shape(RawModel rm, ModelTexture modelTexture){
		this.setup(rm, modelTexture);
	}
	Shape(ModelData md, ModelTexture modelTexture){
		this.setup(new RawModel(md), modelTexture);
	}
	private void setup(RawModel rawModel, ModelTexture modelTexture){
		this.rawModel=rawModel;
		if(modelTexture == null){
			modelTexture = new ModelTexture(ZData.loader.loadTexture("noTextureProvided"));
		}
		this.modelTexture=modelTexture;
		this.texturedModel= new TexturedModel(rawModel, modelTexture);
	}
	public static void prepareAllDataOnShapes(){
		Shape.MODEL_DRAGON.modelTexture.setReflectivity(1);
		Shape.MODEL_DRAGON.modelTexture.setShineDamper(10);
		Shape.MODEL_BUNNY.modelTexture.setReflectivity(1);
		Shape.MODEL_BUNNY.modelTexture.setShineDamper(5);
		Shape.MODEL_FERN.modelTexture.setNumberOfRows(2);
		Shape.MODEL_FERN.modelTexture.setHasTransparency(true);
		Shape.MODEL_WEEDS.modelTexture.setHasTransparency(true);
		Shape.MODEL_WEEDS.modelTexture.setUseFakeNormalVectorLighting(true);
		Shape.MODEL_FLOWER.modelTexture.setHasTransparency(true);
		Shape.MODEL_FLOWER.modelTexture.setUseFakeNormalVectorLighting(true);
		
	}
}
