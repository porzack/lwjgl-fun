package z3d.jy89hui.toolbox;

import graphics.fileLoading.Loader;

public class ZData {
	public static final int WIDTH = 1920;
	public static final int HEIGHT = 1150;
	public static final int FPS_CAP = 120;
	public static final float FOV = 70;
	public static final float NEAR_PLANE = 1.0f;
	public static final float FAR_PLANE= 20000;
	public static float SKY_R=0.4f;
	public static float SKY_G=0.5f;
	public static float SKY_B=0.6f;
	public static final float SKY_ROTATION_SPEED = 0.2f;
	public static final float SKYBOX_SIZE=10000f;
	public static final float  SKYBOX_BLENDFACTOR = 0f;
	public static final float GRAVITY = 5.8f;
	public static final float DRAG = 0.5f;
	public static final int RANDOM_SEED = 4324902;
	public static final int TERRAIN_GENERIC_SIZE = 3500;
	public static final int TERRAIN_VERTEX_COUNT = 512;
	public static final float  TERRAIN_MAX_HEIGHT = 1300;

	
	public final static Loader loader = new Loader();
}
