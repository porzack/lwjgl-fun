package physical.player;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import graphics.entities.Entity;
import graphics.entities.MovingEntity;
import graphics.models.TexturedModel;
import graphics.renderEngine.DisplayManager;
import jutils.jy89hui.general.logger;
import physical.world.World;
import z3d.jy89hui.toolbox.ZData;

public class Player extends MovingEntity{
	
	public static final float PLAYER_HEIGHT = 10;
	private static final float WALK_SPEED = 30; // M/s
	private static final float RUN_SPEED = 4; // M/s
	private static final float TURN_SPEED = 160; // Deg per s
	private static final float JUMP_POWER = 20; // M
	
	
	private float currentSpeed = 0;
	private float currentTurnSpeed=0;
	
	public Player(World world, TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super(world, model, position, rotX, rotY, rotZ, scale);
	}
	boolean paused = false;
	public void move(){
		this.performWASDandEyeMovement();
		if(!paused){
				//this.applyGravity();
				this.applyVelocityToPosition(world,true);
				this.applyDrag(3);
		}
		//logger.log("PlayerPos: ("+position.x+", "+position.y+", "+position.z+")");
	}
	private void performWASDandEyeMovement(){
		float moveNum = WALK_SPEED*DisplayManager.getLastFrameTimeSeconds();
		float jumpPower = JUMP_POWER*DisplayManager.getLastFrameTimeSeconds();
		float angleChange = TURN_SPEED*DisplayManager.getLastFrameTimeSeconds();
		if (Keyboard.isKeyDown(Keyboard.KEY_W)){
			super.increaseVelocity(
			(float)(moveNum*Math.cos(Math.toRadians(yaw-90))),
			0f,
			(float)(moveNum*Math.sin(Math.toRadians(yaw-90))));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_D)){
			super.increaseVelocity(
			(float)(moveNum*Math.cos(Math.toRadians(yaw))),
			0f,
			(float)(moveNum*Math.sin(Math.toRadians(yaw))));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_A)){
			super.increaseVelocity(
			(float)(moveNum*Math.cos(Math.toRadians(yaw+180))),
			0f,
			(float)(moveNum*Math.sin(Math.toRadians(yaw+180))));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_S)){
			super.increaseVelocity(
			(float)(moveNum*Math.cos(Math.toRadians(yaw+90))),
			0f,
			(float)(moveNum*Math.sin(Math.toRadians(yaw+90))));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			velocity.y+=jumpPower;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_P)){
			paused=!paused;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
			velocity.y-=jumpPower;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)){
			pitch+=angleChange;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)){
			pitch-=angleChange;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
			yaw-=angleChange;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
			yaw+=angleChange;
		}
	}

}
