package physical.gameObjects;

import org.lwjgl.util.vector.Vector3f;

import graphics.entities.MovingEntity;
import graphics.models.TexturedModel;
import physical.world.World;

public class Projectile extends MovingEntity{

	public Projectile(World world, TexturedModel model, Vector3f position, Vector3f velocity, float rotX, float rotY, float rotZ,
			float scale) {
		super(world, model, position, velocity, rotX, rotY, rotZ, scale);
	}

	@Override
	public void move() {
		this.applyGravity();
		if(this.applyVelocityToPosition(world, true)) {
			this.applyDrag();
		}
		
	}



}
