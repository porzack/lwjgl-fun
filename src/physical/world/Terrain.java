package physical.world;

import java.awt.image.BufferedImage;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import graphics.fileLoading.Loader;
import graphics.models.RawModel;
import graphics.textures.ModelTexture;
import graphics.textures.TerrainTexture;
import graphics.textures.TerrainTexturePack;
import jutils.jy89hui.file.FileInterface;
import jutils.jy89hui.general.LoggerMode;
import jutils.jy89hui.general.logger;
import z3d.jy89hui.toolbox.ZData;
import z3d.jy89hui.toolbox.Tools;

public class Terrain {
	protected static final float SIZE = ZData.TERRAIN_GENERIC_SIZE;
	protected static final float MAX_HEIGHT = ZData.TERRAIN_MAX_HEIGHT;
	protected static final float MIN_HEIGHT = 0;
	private static final float MAX_PIXEL_COLOR = 256*256*256;
	
	private float[][] heights;
	
	private float x,z;
	private int gridx, gridz;
	private RawModel model;
	private TerrainTexturePack texturePack;
	private TerrainTexture blendMap;
	public Terrain(int gridx, int gridz, TerrainTexturePack texture, TerrainTexture blendMap, String heightMap){
		this.texturePack = texture;
		this.blendMap = blendMap;
		this.gridx=gridx;
		this.gridz=gridz;
		this.x=gridx *SIZE;
		this.z =  gridz * SIZE;
		this.model = generateTerrainHeightmap(ZData.loader, heightMap);
	}
	public Terrain(int gridx, int gridz, TerrainTexturePack texture, TerrainTexture blendMap, HeightmapGenerator generator){
		this.texturePack = texture;
		this.blendMap = blendMap;
		this.gridx=gridx;
		this.gridz=gridz;
		this.x=gridx *SIZE;
		this.z =  gridz * SIZE;
		this.model = generateTerrainRandom(ZData.loader, generator);
	}
	
	public int getGridx() {
		return gridx;
	}

	public void setGridx(int gridx) {
		this.gridx = gridx;
	}

	public int getGridz() {
		return gridz;
	}

	public void setGridz(int gridz) {
		this.gridz = gridz;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public RawModel getRawModel() {
		return model;
	}

	public void setRawModel(RawModel model) {
		this.model = model;
	}

	public TerrainTexturePack getTexturePack() {
		return texturePack;
	}

	public TerrainTexture getBlendMap() {
		return blendMap;
	}

	public float getHeight(int worldX, int worldZ){
		float terrainX = worldX-this.x;
		float terrainZ = worldZ-this.z;
		float gridSquareSize = SIZE / ((float)heights.length-1);
		int gridX = (int)Math.floor(terrainX/gridSquareSize);
		int gridZ = (int)Math.floor(terrainZ/gridSquareSize);
		if(gridX >= heights.length-1 || gridZ >= heights.length-1 || gridX<0 || gridZ<0){
			logger.log("The position: "+x+" "+z+" is not present on the terrain ("+x+","+z+")",LoggerMode.TYPE_WARNING);
			return Float.MIN_VALUE;
		}
		float xCoord = (terrainX % gridSquareSize)/gridSquareSize;
		float zCoord = (terrainZ % gridSquareSize)/gridSquareSize;
		float result;
		if (xCoord <= (1-zCoord)) {
			result = Tools
					.barryCentric(new Vector3f(0, heights[gridX][gridZ], 0), 
							new Vector3f(1,heights[gridX + 1][gridZ], 0), 
							new Vector3f(0, heights[gridX][gridZ + 1], 1), 
							new Vector2f(xCoord, zCoord));
		} else {
			result = Tools
					.barryCentric(new Vector3f(1, heights[gridX + 1][gridZ], 0),
							new Vector3f(1,heights[gridX + 1][gridZ + 1], 1), 
							new Vector3f(0,heights[gridX][gridZ + 1], 1), 
							new Vector2f(xCoord, zCoord));
		}
		return result;
	}
	public Vector3f getNormal(int wX, int wZ){
		float h00=getHeight(wX+0,wZ+0);
		float h10=getHeight(wX+1,wZ+0);
		float h01=getHeight(wX+0,wZ+1);
		float hm10=getHeight(wX-1,wZ+0);
		float h0m1=getHeight(wX+0,wZ-1);
		
		float r = (h10-hm10)/2f;
		float p = (h01-h0m1)/2f;
		
		r*=50;
		p*=-50;
		
		Vector3f normal = new Vector3f(p,0,r);
		logger.log("("+p+", "+r+")");
		
		return normal;
		
		//return result;
	}
	private RawModel generateTerrainHeightmap(Loader loader, String heightMap){
		BufferedImage image = FileInterface.LOAD_IMAGE("res/"+heightMap+".png");
		int VERTEX_COUNT = image.getHeight();
		heights = new float[VERTEX_COUNT][VERTEX_COUNT];
		int count = VERTEX_COUNT * VERTEX_COUNT;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		int[] indices = new int[6*(VERTEX_COUNT-1)*(VERTEX_COUNT-1)];
		int vertexPointer = 0;
		for(int i=0;i<VERTEX_COUNT;i++){
			for(int j=0;j<VERTEX_COUNT;j++){
				vertices[vertexPointer*3] = (float)j/((float)VERTEX_COUNT - 1) * SIZE;
				float height = getHeight(j,i,image);
				heights[j][i]=height;
				vertices[vertexPointer*3+1] = height;
				vertices[vertexPointer*3+2] = (float)i/((float)VERTEX_COUNT - 1) * SIZE;
				Vector3f normal = calculateNormal(j,i,image);
				normals[vertexPointer*3] = normal.x;
				normals[vertexPointer*3+1] = normal.y;
				normals[vertexPointer*3+2] = normal.z;
				textureCoords[vertexPointer*2] = (float)j/((float)VERTEX_COUNT - 1);
				textureCoords[vertexPointer*2+1] = (float)i/((float)VERTEX_COUNT - 1);
				vertexPointer++;
			}
		}
		int pointer = 0;
		for(int gz=0;gz<VERTEX_COUNT-1;gz++){
			for(int gx=0;gx<VERTEX_COUNT-1;gx++){
				int topLeft = (gz*VERTEX_COUNT)+gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1)*VERTEX_COUNT)+gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}
		return loader.loadToVAO(vertices, textureCoords, normals, indices);
	}
	private RawModel generateTerrainRandom(Loader loader, HeightmapGenerator generator){
		
		int VERTEX_COUNT = ZData.TERRAIN_VERTEX_COUNT;
		heights = new float[VERTEX_COUNT][VERTEX_COUNT];
		int count = VERTEX_COUNT * VERTEX_COUNT;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		int[] indices = new int[6*(VERTEX_COUNT-1)*(VERTEX_COUNT-1)];
		int vertexPointer = 0;
		for(int i=0;i<VERTEX_COUNT;i++){
			for(int j=0;j<VERTEX_COUNT;j++){
				vertices[vertexPointer*3] = (float)j/((float)VERTEX_COUNT - 1) * SIZE;
				float height = getHeight(j,i,generator);
				heights[j][i]=height;
				vertices[vertexPointer*3+1] = height;
				vertices[vertexPointer*3+2] = (float)i/((float)VERTEX_COUNT - 1) * SIZE;
				Vector3f normal = calculateNormal(j,i,generator);
				normals[vertexPointer*3] = normal.x;
				normals[vertexPointer*3+1] = normal.y;
				normals[vertexPointer*3+2] = normal.z;
				textureCoords[vertexPointer*2] = (float)j/((float)VERTEX_COUNT - 1);
				textureCoords[vertexPointer*2+1] = (float)i/((float)VERTEX_COUNT - 1);
				vertexPointer++;
			}
		}
		int pointer = 0;
		for(int gz=0;gz<VERTEX_COUNT-1;gz++){
			for(int gx=0;gx<VERTEX_COUNT-1;gx++){
				int topLeft = (gz*VERTEX_COUNT)+gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1)*VERTEX_COUNT)+gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}
		return loader.loadToVAO(vertices, textureCoords, normals, indices);
	}
	private Vector3f calculateNormal(int x, int z, BufferedImage image){
		
		float heightL = getHeight(x-1,z,image);
		float heightR = getHeight(x+1,z,image);
		float heightD = getHeight(x,z-1,image);
		float heightU = getHeight(x,z+1,image);
		Vector3f normal = new Vector3f(heightL-heightR, 2f, heightD-heightU);
		normal.normalise();
		return normal;
	}
	private Vector3f calculateNormal(int x, int z, HeightmapGenerator generator){
		
		float heightL = getHeight(x-1,z,generator);
		float heightR = getHeight(x+1,z,generator);
		float heightD = getHeight(x,z-1,generator);
		float heightU = getHeight(x,z+1,generator);
		Vector3f normal = new Vector3f(heightL-heightR, 2f, heightD-heightU);
		normal.normalise();
		return normal;
	}
	private float getHeight(int x, int z, BufferedImage image){
		if(x<0 || x>=image.getHeight() || z<0 || z>=image.getHeight()){
			return 0;
		}
		try{
			float height = image.getRGB(x, z);
			height += MAX_PIXEL_COLOR;
			height /= MAX_PIXEL_COLOR;
			height *= MAX_HEIGHT;
			return height;
		}catch(Exception e){
			logger.log("Heightmap height determination failed: "+x+" "+z,LoggerMode.TYPE_WARNING);
			e.printStackTrace();
			return 0;
		}
	}
	private float getHeight(int x, int z, HeightmapGenerator generator){
		return generator.generateHeight(x, z);
	}
}
