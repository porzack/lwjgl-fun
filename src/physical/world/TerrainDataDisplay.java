package physical.world;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;


public class TerrainDataDisplay extends BasicGame{

	private TerrainData testData = new TerrainData();
	public TerrainDataDisplay() {
		super("Terrain Data Display");
	}
	public static void main(String[] args) throws SlickException{
		AppGameContainer app = new AppGameContainer(new TerrainDataDisplay());
		app.setDisplayMode(800, 800, false);
		app.setAlwaysRender(true);
		app.setShowFPS(false);
		app.start();
	}

	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		g.setColor(Color.white);
		g.drawString("Number of points: "+testData.getData().values().size(), 20, 20);
		for(int[] point : testData.getData().values()){
			int color = point[TerrainData.POINT_Y];
			g.setColor(new Color(color,color,color));
			g.fillOval(point[TerrainData.POINT_X], point[TerrainData.POINT_Z], 5, 5);
			for(int other : testData.getConnectedPoints(point)){
				if(other == testData.EMPTY){
					continue;
				}
				int[] otherPoint = testData.getPointData(other);
				g.drawLine(point[TerrainData.POINT_X], point[TerrainData.POINT_Z],
						otherPoint[TerrainData.POINT_X], otherPoint[TerrainData.POINT_Z]);
			}
		}
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		int point1  = testData.registerPoint(testData.generatePoint(50, 255, 60, 0, 0));
		int point2 = testData.registerPoint(testData.generatePoint(330, 255, 60, 0, 0));
		if(testData.getFreeSpaceForConnections(point1)<1){
			testData.expandRegisteredPoint(point1, 1);
		}
		if(testData.getFreeSpaceForConnections(point2)<1){
			testData.expandRegisteredPoint(point2, 1);
		}
		testData.connectPoints(point1, point2);
	//	testData.registerPoint(point);
		//testData.registerPoint(point2);
		
		testData.generateFlatTerrain(100, 100, 150, 110, 200);
		
		ArrayList<Integer> points = new ArrayList<Integer>();

		for(int i=0; i<100; i++){
			int x = (int)(Math.random()*800.0);
			int z = (int)(Math.random()*800.0);
			points.add(testData.registerPoint(testData.generatePoint(x, 200, z, 0, 0)));
		}
		for(int x=0; x<100; x++){
			for(int i :points){
				int other = points.get((int)(Math.random()*100.0));
				testData.connectPoints(i,other);
			}
		}
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		// TODO Auto-generated method stub
		
	}

}
