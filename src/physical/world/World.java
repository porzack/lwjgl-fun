package physical.world;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;

import graphics.entities.Entity;
import graphics.entities.MovingEntity;
import jutils.jy89hui.general.logger;
import physical.player.Player;

public class World {

	private List<Terrain> terrains;
	private List<Entity> entities;
	private Player player;
	public World(){
		terrains = new ArrayList<Terrain>();
		entities = new ArrayList<Entity>();
	}
	public void addTerrain(Terrain t){
		if(getTerrainGrid(t.getGridx(),t.getGridz())==null){
			terrains.add(t);
		}else{
			logger.warn("Terrain ("+t.getGridx()+", "+t.getGridz()+") not added. Already present. ");
		}
		
	}
	public List<Terrain> getTerrains(){
		return this.terrains;
	}
	public float getHeight(float x, float z){
		int x1 = (int)x;
		int z1 = (int)z;
		Terrain t = this.getTerrainWorld(x1, z1);
		if (t==null){
			return Terrain.MIN_HEIGHT;
		}
		return t.getHeight(x1, z1);
	}
	public Vector3f getNormal(float x, float z){
		int x1 = (int)x;
		int z1 = (int)z;
		Terrain t = this.getTerrainWorld(x1, z1);
		if (t==null){
			return (new Vector3f(0f,0f,0f));
		}
		return t.getNormal(x1, z1);
	}
	private Terrain getTerrainGrid(int x, int z){
		for (Terrain t : terrains){
			if(t.getGridx()==x && t.getGridz() == z){
				return t;
			}
		}
		return null;
	}
	private Terrain getTerrainWorld(int worldx, int worldz){
		float x1 = worldx/Terrain.SIZE;
		float z1 = worldz/Terrain.SIZE;
		x1=(int) Math.floor(x1);
		z1=(int) Math.floor(z1);
		return getTerrainGrid((int)x1,(int)z1);
	}
	public void addEntity(Entity e){
		entities.add(e);
	}
	public List<Entity> getEntities(){
		return this.entities;
	}
	public void setPlayer(Player p){
		player = p;
	}
	public Player getPlayer(){
		return player;
	}
	public void performPhysicalUpdates(){
		player.move();
		for (Entity e : entities) {
			if (e instanceof MovingEntity) {
				MovingEntity movingEntity = (MovingEntity)e;
				movingEntity.move();
			}
		}
	}
	
}
