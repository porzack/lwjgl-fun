package physical.world;

import java.util.HashMap;
import java.util.Map;

import jutils.jy89hui.general.LoggerMode;
import jutils.jy89hui.general.logger;
import jutils.jy89hui.objectTools.ArrayTools;

public class TerrainData {
	/* PHYSICAL POINT DEFINITION: 
	 * Terrain Method: Triangles
	 * int[]
	 * id
	 * x
	 * y
	 * z
	 * terrainType
	 * Begin point branches.
	 * End point branches at end of list 
	 */
	public static final int EMPTY = -1;
	public static final int VAL_X_MAX = Integer.MAX_VALUE;
	public static final int VAL_Y_MAX = Integer.MAX_VALUE;
	public static final int VAL_Z_MAX = Integer.MAX_VALUE;
	public static final int POINT_ID=0;
	public static final int POINT_X=1;
	public static final int POINT_Y=2;
	public static final int POINT_Z=3;
	public static final int POINT_TERRAIN_TYPE=4;
	public static final int POINT_BEGIN_BRANCHES=5;
	private Map<Integer,int[]> physicalPoints = new HashMap<Integer,int[]>();
	
	
	public int getPointData(Integer point, int dataID){
		return physicalPoints.get(point)[dataID];
	}
	public int[] getPointData(Integer point){
		return physicalPoints.get(point);
	}
	public Map<Integer, int[]> getData(){
		return this.physicalPoints;
	}
	public int[] generatePoint(int x, int y, int z, int type, int number_of_points){
		int[] data = generateArray(5+number_of_points);
		ArrayTools.fill(data, EMPTY);
		data[POINT_X]=x;
		data[POINT_Y]=y;
		data[POINT_Z]=z;
		data[POINT_TERRAIN_TYPE]=type;
		if(data[POINT_ID]==EMPTY){
			this.genLocationIdentifier(data);
		}
		return data;
	}
	
	public int registerPoint(int[] data){
		
		if(data[POINT_ID]==EMPTY){
			genLocationIdentifier(data);
		}
		physicalPoints.put(data[POINT_ID], data);
		logger.log(
				"TerrainData: Registered Point ID: "+toString(data)
				, LoggerMode.TYPE_DEBUG);
		return data[POINT_ID];
	}
	private int[] genLocationIdentifier(int[] data){
		long key = generateLocationIdentifier(data[POINT_X], data[POINT_Y],data[POINT_Z]);
		if(key>=Integer.MAX_VALUE){
			logger.fatalError("Cannot generate key value, value greater than int max val: "+key);
		}
		data[POINT_ID] = (int)key;
		return data;
	}
	private int[] insertConnectedPoint(int[] data, int otherPoint){
		for(int i=POINT_BEGIN_BRANCHES; i<data.length; i++){
			if(data[i]==EMPTY){
				data[i]=otherPoint;
				return data;
			}
		}
		data = expandArray(data, 1);
		data[data.length-1]=otherPoint;
		return data;
	}
	private int[] insertConnectedPoint(int[] data, int[] otherpoint){
		return insertConnectedPoint(data,otherpoint[POINT_ID]);
	}
	public void connectPoints(int p1, int p2){
		if(this.getFreeSpaceForConnections(p1)<1){
			this.expandRegisteredPoint(p1, 1);
		}
		if(this.getFreeSpaceForConnections(p2)<1){
			this.expandRegisteredPoint(p1, 1);
		}
		int[] point1 = physicalPoints.get(p1);
		int[] point2 = physicalPoints.get(p2);
		insertConnectedPoint(point1,point2);
		insertConnectedPoint(point2,point1);
		logger.log("TerrainData: Connected Points "+toString(p1)+", "+toString(p2),LoggerMode.TYPE_DEBUG);
	}
	public void deletePoint(Integer key){
		physicalPoints.remove(key);
	}
	private int[] generateArray(int length){
		return new int[length];
	}
	public void expandRegisteredPoint(int point, int amount){
		int[] arry = physicalPoints.get(point);
		int[] newPoint = expandArray(arry, amount);
		registerPoint(newPoint);
		logger.log("Expanded Registered Point "+toString(newPoint)+" to size "+newPoint.length,LoggerMode.TYPE_DEBUG);
	}
	public int[] expandArray(int[] point, int amount){
		int length = point.length+amount;
		int[] toReturn = new int[length];
		ArrayTools.fill(toReturn, EMPTY);
		for(int i=0;i<point.length; i++){
			toReturn[i]=point[i];
		}
		return toReturn;
	}
	public int[] getConnectedPoints(int[] point){
		int[] toReturn = new int[point.length-POINT_BEGIN_BRANCHES];
		for(int i=POINT_BEGIN_BRANCHES; i<point.length;i++){
				toReturn[i-POINT_BEGIN_BRANCHES]=point[i];
		}
		return toReturn;
	}
	public int getFreeSpaceForConnections(int point){
		int[] arry = physicalPoints.get(point);
		int amount =0;
		for(int i=POINT_BEGIN_BRANCHES; i<arry.length;i++){
				if(arry[i]==EMPTY){
					amount++;
				}
		}
		return amount;
	}
	public void generateFlatTerrain(int x,int y, int z, int width, int length){
		int point0 = registerPoint(generatePoint(x,y,z,0,2));
		int point1 = registerPoint(generatePoint(x+width,y,z,0,2));
		int point2 = registerPoint(generatePoint(x,y,z+length,0,2));
		int point3 = registerPoint(generatePoint(x+width,y,z+length,0,2));
		connectPoints(point0,point1);
		connectPoints(point0,point2);
		connectPoints(point1, point2);
		connectPoints(point3,point1);
		connectPoints(point3,point2);
	}
	public static long generateLocationIdentifier(int x, int y, int z){
		long x1 = toNaturalSet(x);
		long y1 = toNaturalSet(y);
		long z1 = toNaturalSet(z);
		try{
			return (generatePair(x1,z1));
		}
		catch (Exception e){
			e.printStackTrace();
			logger.fatalError("Class: TerrainData. Error: There has been an error generating a proper identifier for the key pair ("+x+", "+y+", "+z+").");
			return 0;
		}
	}
	private static long generatePair(long a, long b){
		return (long)((float)(0.5)*((float)a+(float)b)*(a+b+1.0f)+b);
	}
	private static long toNaturalSet(int val){
		//return Math.abs(val);
		if(val>=0){
			return (long) (val*2);
		}else{
			return (long) ((-val*2)-1);
		}
	}
	public String toString(int[] point){
		return "(ID:"+point[POINT_ID]+", x:"+point[POINT_X]+", y:"+point[POINT_Y]+", z:"+point[POINT_Z]+" type:"+point[POINT_TERRAIN_TYPE]+", Connections: "+(point.length-POINT_BEGIN_BRANCHES)+")";
	}
	public String toString(int point){
		return toString(physicalPoints.get(point));
	}
}
